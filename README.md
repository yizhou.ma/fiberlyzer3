# Fiberlyzer3

## Description

This is a software developed by the Food Process Engineering Chair Group at Wageningen University, the Netherlands. The Fiberlyzer software performs image analysis to assess fibrous structures of plant-based meat analogues. 

## Installation

The current version of software can only be run with the required python modules installed (see requirements.txt for details). We recommend running the scripts in PyCharm or other IDE. Pull this repository to start using the software.

## Image requirements

Example images can be found in the `Example image` folder. The images are cropped to focus on the inner structure of a folded plant-based meat analogue. The image should be taken free of surface moisture, and it should be free of visible cracks and edges from breaking the sample open (soometimes they are referred as 'skins'of the meat analogue).

## Navigating the UI

Run the `main_UI.py` to start the program. `Open to browse` an image for the Fiberlyzer analysis. `HSV Step`, `Threshold`, and `Minimal fiber area (%)` are image processing parameters that were optimized for the image set collected in our study. These parameters may need to be adjustedt if images are taken using different lighting conditions and camera settings. Check the `Show mask`option to visualize the selected fiber areas on the image. You can adjust the parameters and access how well the fiber areas are segmented. 

`Fiberlyze` button runs the assessment pipeline, and the analysis results are shown in the status bar on the bottom of the UI panel. More detailed results can be visualized by the `Plot results` button, and results can be saved using the `Save results` button. 


## Bug report and contributions

For bugs and intent for contritbutions of this software, please reach out at [yizhou.ma@wur.nl](yizhou.ma@wur.nl).

## Reference

If you found this software useful, it will be great if you can cite our publication: Ma, Y., Schlangen, M., Potappel, J., Zhang, L., & van der Goot, A. J. (2023). [Quantitative characterizations of visual fibrousness in meat analogues using automated image analysis](https://doi.org/10.1111/jtxs.12806). Journal of Texture Studies, 1–12.

## Example survey

You can find an example of the survey we used to validate the method [here](https://git.wur.nl/yizhou.ma/fiberlyzer3/-/blob/main/Qualtrics_Survey_Fiberlyzer.pdf).


