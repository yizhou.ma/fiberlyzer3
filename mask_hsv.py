import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.colors as plt_colors
import csv
import timeit
import argparse
import os
import pywt
from skimage.transform import resize
from skimage.morphology import medial_axis, skeletonize, binary_opening
import functions

def create_hsv_mask(img, step, level=None):
    img_hsv = cv2.GaussianBlur(img, (15, 15), 0, 0)
    img_hsv = cv2.cvtColor(img_hsv, cv2.COLOR_BGR2HSV)
    #functions.plot_figure(img_hsv)
    img_hsv = quantize_hsv(img_hsv, step)
    if level:
        img_hsv[img_hsv <= np.unique(img_hsv)[level]] = 0
        img_hsv[img_hsv >= 1] = 1
    return img_hsv

def quantize_hsv(img, step):
    hsv_range = np.arange(255, 0, -step)
    hsv_range = hsv_range[::-1]
    images_hsv = []
    for r in hsv_range:
        l_b = np.array([0, max(r - (step - 1), 0), 0])
        u_b = np.array([255, r, 255])
        mask = cv2.inRange(img, l_b, u_b)
        images_hsv.append(mask)

    mask_hsv = np.zeros_like(images_hsv[0])
    for i, img_hsv in enumerate(images_hsv):
        img_hsv[img_hsv != 0] = 255 - (i * step)
        mask_hsv = mask_hsv + img_hsv
    return mask_hsv

def nothing(x):
    pass

def HSV_segment(frame):
    result = False
    frame_copy = frame.copy()
    frame_copy = cv2.GaussianBlur(frame_copy, (15, 15), 0, 0)

    hsv = cv2.cvtColor(frame_copy, cv2.COLOR_BGR2HSV)
    H,S,V = cv2.split(hsv)
    S_95 = np.percentile(S, 70)
    print(S_95)
    cv2.namedWindow("Tracking", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
    cv2.createTrackbar("LH", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("LS", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("LV", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("UH", "Tracking", 255, 255, nothing)
    cv2.createTrackbar("US", "Tracking", 255, 255, nothing)
    cv2.createTrackbar("UV", "Tracking", 255, 255, nothing)
    cv2.namedWindow("mask", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)

    while True:
        l_h = cv2.getTrackbarPos("LH", "Tracking")
        l_s = cv2.getTrackbarPos("LS", "Tracking")
        l_v = cv2.getTrackbarPos("LV", "Tracking")

        u_h = cv2.getTrackbarPos("UH", "Tracking")
        u_s = cv2.getTrackbarPos("US", "Tracking")
        u_v = cv2.getTrackbarPos("UV", "Tracking")

        l_b = np.array([l_h, l_s, l_v])
        u_b = np.array([u_h, u_s, u_v])

        mask = cv2.inRange(hsv, l_b, u_b)
        cv2.imshow("mask", mask)
        key = cv2.waitKey(1)

        if key == 27 or key == ord('q'):
            cv2.destroyAllWindows()
            threshed_orig = mask.copy()
            result = True
            print('Segmented image')
            cv2.imwrite('Segmented image.png', mask)
            break
    return threshed_orig, result
