import cv2
import numpy as np
import timeit
from skimage.morphology import medial_axis, skeletonize, binary_opening
from itertools import combinations
import functions
import math

def skeletonize_count_shortest_path(info, mask):
    # check if there are components in the mask
    #if info[0] == 0:
    #    return

    mask[mask > 0] = 1
    medial_img, medial_width = medial_axis(mask, return_distance=True)
    skel = skeletonize(mask, method='lee')

    # All 8 directions
    delta = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]

    blob_area = []
    rect_width = []
    rect_height = []
    rect_factors = []
    rect_fills = []

    skel_length = []
    skel_width = []
    skel_factor = []
    skel_shape = []
    for stat, i in zip(info[2], info[3]):
        # Draw the smallest rectangle possible around the isolated shape
        mask_i = np.zeros_like(info[1], dtype=np.uint8)
        mask_i[info[1] == i] = 255
        contours, hierarchy = cv2.findContours(mask_i, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        cnts = []
        for cntrs in contours:
            for cnt in cntrs:
                cnts.append(cnt)
        cnts = np.array(cnts)
        rect = cv2.minAreaRect(cnts)

        area = stat[cv2.CC_STAT_AREA]
        rect_w = rect[1][0]
        rect_h = rect[1][1]
        rect_factor = rect_h / rect_w
        rect_fill = area / (rect_w * rect_h) * 100

        blob_area.append(area)
        rect_width.append(rect_w)
        rect_height.append(rect_h)
        rect_factors.append(rect_factor)
        rect_fills.append(rect_fill)

        # Skeleton info
        skel_i = np.zeros_like(info[1], dtype=np.uint8)
        skel_i[(info[1] == i)] = skel[(info[1] == i)]
        img_conv = cv2.filter2D(skel_i.astype(np.uint8), -1, np.ones((3, 3)))  #
        img_conv = img_conv * skel_i
        img_tips = img_conv == 2
        tips = np.array(np.nonzero(img_tips)).T
        tip_combs = combinations(tips, 2)  # get all the combinations of the tips in case the skeleton are branched
        tip_combs = list(tip_combs)
        if len(tip_combs) > 400:
            print(f"doing {i} out of {info[3][-1]} with length {len(tip_combs)}:")
            starttime = timeit.default_timer()
        longest_path = []
        for tip_comb in list(tip_combs):
            start, end = tuple(tip_comb[0]), tuple(tip_comb[1])
            paths = functions.findPathBwTwoPoints(skel_i, delta, points=[start, end])  # this will return the path between the start and end points
            # fig,ax = plt.subplots(1, figsize=(15,15))
            # ax.imshow(skel_i,'gray')
            # ax.scatter( [paths[0][1],paths[-1][1]],
            #          [paths[0][0],paths[-1][0]],s=10,c='r')
            # paths[:, [0, 1]] = paths[:, [1, 0]]
            if len(paths) > len(longest_path):
                longest_path = paths
        if len(tip_combs) > 400:
            print("The time difference is :", timeit.default_timer() - starttime)



        longest_transposed = np.array(longest_path).T.tolist()
        width_skel = medial_width[tuple(longest_transposed)]
        length = len(longest_path)
        width = 2 * np.average(width_skel)

        skel_length.append(length)
        skel_width.append(width)
        skel_factor.append(length / width)
        skel_shape.append(len(tips))

    return [skel_length, skel_width, skel_factor, skel_shape, blob_area, rect_width, rect_height, rect_factors, rect_fills]


def skeletonize_count_px_length(info, mask):
    # check if there are components in the mask
    #if info[0] == 0:
    #    return

    mask[mask > 0] = 1
    medial_img, m_distance = medial_axis(mask, return_distance=True)
    skeleton_img = skeletonize(mask, method='lee')
    skeleton_img = skeleton_img * m_distance

    skel_length = []
    skel_width = []
    skel_factor = []
    for i in info[3]:
        skel_i = np.zeros_like(info[1], dtype=float)
        skel_i[(info[1] == i)] = skeleton_img[(info[1] == i)]
        width = np.average(skel_i[np.nonzero(skel_i)])
        length = np.count_nonzero(skel_i)
        skel_length.append(length)
        skel_width.append(width)
        skel_factor.append((length / width))
    return [skel_length, skel_width, skel_factor]

def skeletonize_smallest_box(info):
    items = []
    # check if there are components in the mask
    if info[0] == 0:
        return

    for stat, i in zip(info[2], info[3]):
        w = stat[cv2.CC_STAT_WIDTH]
        h = stat[cv2.CC_STAT_HEIGHT]
        area = stat[cv2.CC_STAT_AREA]
        factor = w / h

        mask_i = np.zeros_like(info[1], dtype=np.uint8)
        mask_i[info[1] == i] = 255
        contours, hierarchy = cv2.findContours(mask_i, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        cnts = []
        for cntrs in contours:
            for cnt in cntrs:
                cnts.append(cnt)
        cnts = np.array(cnts)
        rect = cv2.minAreaRect(cnts)

        min_w = rect[1][0]
        min_h = rect[1][1]
        min_factor = min_w / min_h
        min_fill = 100 / (min_w * min_h) * area

        items.append([i, h, w, area, factor, min_w, min_h, min_factor, min_fill])

    return np.array(items)

def skeletonize_count_shortest_path2(info, mask,area_threshold):
    # check if there are components in the mask
    #if info[0] == 0:
    #    return

    mask[mask > 0] = 1
    medial_img, medial_width = medial_axis(mask, return_distance=True)
    skel = skeletonize(mask, method='lee')
    h, w = mask.shape
    total_area = h*w
    # All 8 directions
    delta = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]

    blob_area = []

    skel_length = []
    skel_width = []
    skel_factor = []
    skel_shape = []
    for stat, i in zip(info[2], info[3]):
        #find the area of the blob
        area = stat[cv2.CC_STAT_AREA]
        if area >= area_threshold*total_area:

            # Skeleton info
            skel_i = np.zeros_like(info[1], dtype=np.uint8)
            skel_i[(info[1] == i)] = skel[(info[1] == i)]
            img_conv = cv2.filter2D(skel_i.astype(np.uint8), -1, np.ones((3, 3)))  #
            img_conv = img_conv * skel_i
            img_tips = img_conv == 2
            tips = np.array(np.nonzero(img_tips)).T
            tip_combs = combinations(tips, 2)  # get all the combinations of the tips in case the skeleton are branched
            tip_combs = list(tip_combs)
            if len(tips) > 20:
                print(f'Skipping blob with {len(tips)} branches')
                continue
            else:
                longest_path = []
                longest_line_path = 1
                for tip_comb in list(tip_combs):
                    start, end = tuple(tip_comb[0]), tuple(tip_comb[1])
                    line_path = abs(start[0]-end[0])+ abs(start[1]-end[1])
                    paths = functions.findPathBwTwoPoints(skel_i, delta, points=[start, end])  # this will return the path between the start and end points
                    if len(paths) > len(longest_path):
                        longest_path = paths
                        longest_line_path = line_path


                longest_transposed = np.array(longest_path).T.tolist()
                width_skel = medial_width[tuple(longest_transposed)]
                length = len(longest_path)
                curl_ratio = round(length / longest_line_path, 2)
                if curl_ratio < 1.2:
                    width = 2 * np.average(width_skel)

                    skel_length.append(length)
                    skel_width.append(width)
                    skel_factor.append(length / width)
                    skel_shape.append(len(tips))

                    blob_area.append(area)
    return [skel_length, skel_width, skel_factor, skel_shape, blob_area]
