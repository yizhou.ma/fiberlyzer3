import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.colors as plt_colors
import csv
import timeit
import argparse
import os
import pywt
from skimage.transform import resize
from skimage.morphology import medial_axis, skeletonize, binary_opening
import mask_hsv
import functions

# if __name__ == "__main__":
#     pass


# loop over the number of unique connected component labels
# inc_first = 1 will skip over background
def loop_labels(mask, numLabels, labels, stats, thresh_v, inc_first=1):
    labels_thresh = []
    for i in range(inc_first, numLabels):
        # extract the connected component statistics and centroid for the current label
        x = stats[i, cv2.CC_STAT_LEFT]
        y = stats[i, cv2.CC_STAT_TOP]
        w = stats[i, cv2.CC_STAT_WIDTH]
        h = stats[i, cv2.CC_STAT_HEIGHT]
        area = stats[i, cv2.CC_STAT_AREA]

        # ensure the width, height, and area are all neither too small nor too big
        keepWidth = w > thresh_v[0][0] and w < thresh_v[0][1]
        keepHeight = h > thresh_v[1][0] and h < thresh_v[1][1]
        keepArea = area > thresh_v[2][0] and area < thresh_v[2][1]
        # ensure the connected component we are examining passes all
        # three tests
        if all((keepWidth, keepHeight, keepArea)):
            # construct a mask for the current connected component and then take the bitwise OR with the mask
            #print("[INFO] keeping connected component '{}'".format(i))
            labels_thresh.append(i)
            componentMask = (labels == i).astype("uint8") * 255
            mask = cv2.bitwise_or(mask, componentMask)
        #else: print("[INFO] not keeping connected component '{}', width '{}', height '{}', area '{}'".format(i,w,h,area))
    return mask, labels_thresh

def connect_components(mask, connectivity, thresh_vars):
    # apply connected component analysis to the thresholded image
    output = cv2.connectedComponentsWithStats(mask, connectivity, cv2.CV_32S)
    (numLabels, labels, stats, centroids) = output
    new_mask = np.zeros_like(mask, dtype="uint8")
    img_mask, labels_thresh = loop_labels(new_mask, numLabels, labels, stats, thresh_vars)
    return img_mask, output, labels_thresh

def show_box_label(mask, level, maskk):
    msk = np.zeros_like(mask)
    msk[(mask == level)] = 255

    plt.figure(figsize=(15, 15))
    plt.imshow(maskk, cmap=plt.cm.gray)
    plt.show()
    plt.figure(figsize=(15, 15))
    plt.imshow(msk, cmap=plt.cm.gray)
    plt.show()

    # contours, hierarchy = cv2.findContours(msk, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    # cnts = []
    # for cntrs in contours:
    #  for cnt in cntrs:
    #    cnts.append(cnt)
    # cnts = np.array(cnts)
    # rect = cv2.minAreaRect(cnts)