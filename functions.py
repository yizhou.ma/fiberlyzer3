import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.colors as plt_colors
import csv
import timeit
import argparse
import os
import pywt
from skimage.transform import resize
from skimage.morphology import medial_axis, skeletonize, binary_opening
import sys
from collections import deque
from itertools import combinations

def plot_single_figure(img, size=(15,5), overlay=[]):
    plt.figure(figsize=size)

    plt.imshow(img)
    if len(overlay) >= 1:
        plt.imshow(overlay, alpha=0.5)

    plt.show()
    plt.close()

def plot_figure(imgs, size=(15,5), overlay=None):
    for img in imgs:
        plt.figure(figsize=size)

        plt.imshow(img)
        if overlay:
            plt.imshow(overlay, alpha=0.5)

    plt.show()
    plt.close()


def quantize_binary(img, step=20, start=0, end=255):
    val_range = np.arange(end, start, -step)
    images_mask = []
    for r in val_range:
        img_mask = img.copy()
        img_mask[img_mask >= r] = 0
        img_mask[img_mask >= 1] = 1
        images_mask.append(img_mask)
    return images_mask

def quantize_binary_rev(img, step=20, start=0, end=255):
    val_range = np.arange(start, end, step)
    images_mask = []
    for r in val_range:
        img_mask = img.copy()
        img_mask[img_mask < r] = 0
        img_mask[img_mask >= 1] = 1
        images_mask.append(img_mask)
    return images_mask

def crop_image(img, border):
    c_vert = border[0]
    c_hor = border[1]
    w = img.shape[1]
    h = img.shape[0]
    if h > c_vert*2 and w > c_hor*2:
        img = img[c_vert:h - c_vert, c_hor:w - c_hor]
    return img

def save_statistical_info(data, filename, header=[]):
    # open the file in the write mode
    with open(filename, 'w', newline='') as f:
        # create the csv writer
        writer = csv.writer(f, delimiter=';')

        if header:
            writer.writerow(header)

        # write a row to the csv file
        for row in range(len(data[0])):
            line = [col[row] for col in data]
            writer.writerow(line)

# BFS
def findPathBwTwoPoints(img_binary, delta, points):
    '''
    img_binary: skeleton image
    points: (y_start_point,x_start_point),(y_end_point,x_end_point)
    '''
    height, width = img_binary.shape

    # The start and end point you're looking at
    # start, end = (31, 14), (34, 51)

    start, end = points


    # Store the results of the BFS as the shortest distance to start
    grid = np.full_like(img_binary, sys.maxsize, dtype=np.uint32)
    grid[start[0], start[1]] = 0

    # The actual BFS algorithm
    bfs = deque([start])
    found = False
    while len(bfs) > 0:
        y, x = bfs.popleft()
        # print(y,x)
        # We've reached the end!
        if (y, x) == end:
            found = True
            break

        # Look all 8 directions for a good path
        for dy, dx in delta:
            yy, xx = y + dy, x + dx
            # If the next position hasn't already been looked at and it's white
            if 0 <= xx < width and 0 <= yy < height and img_binary[yy, xx] != 0 and grid[y, x] + 1 < grid[yy, xx]:
                grid[yy, xx] = grid[y, x] + 1
                bfs.append((yy, xx))

    if found:
        # Now rebuild the path from the end to beginning
        path = []
        y, x = end
        while grid[y, x] != 0:
            for dy, dx in delta:
                yy, xx = y + dy, x + dx
                if 0 <= yy < height and 0 <= xx < width and grid[yy, xx] == grid[y, x] - 1:
                    path.append([yy, xx])
                    y, x = yy, xx
        return path
    else:
        # print(f'No path found between {start} and {end}')
        return 0