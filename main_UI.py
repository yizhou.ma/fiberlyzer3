import cv2
import numpy as np
import mask_hsv
import connected_components
import skeletonizations
import pandas as pd
from tkinter import *
from tkinter import filedialog
import matplotlib.pyplot as plt
window = Tk()
window.title("Fiberlyzer")
window.geometry("600x300")

def is_float(e, var, allow_negative=False):
    n = var.get() + str(e.char)
    if allow_negative and n == "-":
        pass
    else:
        try:
            n = float(n)
        except ValueError:
            if e.char != "\x08" and e.char != "":
                return "break"

def browseFiles():
    filedr = filedialog.askopenfilename(title = "Select a video",
                                        filetypes = (("Picture files",
                                                      "*.jpg* *.png*"),
                                                     ("all files",
                                                      "*.*")))
    newpath.set(filedr.split('/')[-1])
    lb_browse.configure(text="File Opened: " + newpath.get())
    path_fiber.set(filedr)

# Variable list
path_fiber = StringVar(window)
newpath = StringVar(window)
hsv_step = StringVar(window)
hsv_step.set('10')
mask_percentile = DoubleVar(window)
mask_percentile.set(89)
min_area = DoubleVar(window)
min_area.set(0.03)
show_plot = IntVar(window)
show_plot.set(0)

textStatusBar = StringVar(window)
textStatusBar.set("...")


# output list
info_out_df = []

def std_normalize(img):
    img = img.astype(np.float32)

    # Split the image into its color channels
    b, g, r = cv2.split(img)

    # Compute the mean and standard deviation of each channel
    b_mean, b_stddev = cv2.meanStdDev(b)
    g_mean, g_stddev = cv2.meanStdDev(g)
    r_mean, r_stddev = cv2.meanStdDev(r)

    # Normalize each channel by subtracting the mean and dividing by the standard deviation
    b_normalized = (b - b_mean) / b_stddev
    g_normalized = (g - g_mean) / g_stddev
    r_normalized = (r - r_mean) / r_stddev

    # Merge the normalized channels back into a single image
    normalized_img = cv2.merge((b_normalized, g_normalized, r_normalized))

    # Convert the image back to uint8 type
    normalized_img = cv2.convertScaleAbs(normalized_img * 255)

    return normalized_img


def polarized_analysis(img, ksize=5):
    # Apply a Gaussian filter to reduce noise
    img = cv2.GaussianBlur(img, (5, 5), 0)

    # Convert the image to float32 format for calculation
    img = np.float32(img)

    # Calculate the horizontal and vertical derivatives using Sobel filters
    gx = cv2.Sobel(img, cv2.CV_32F, 1, 0, ksize=ksize)
    gy = cv2.Sobel(img, cv2.CV_32F, 0, 1, ksize=ksize)

    # Calculate the phase angle of the polarized light using arctan2 function
    phase_angle = cv2.phase(gx, gy, angleInDegrees=True)
    phase_angle = np.uint8(phase_angle)

    # Calculate the magnitude of the polarized light using the Euclidean norm
    magnitude = cv2.magnitude(gx, gy)
    magnitude = np.uint8(magnitude)

    # Apply thresholding to create a binary image
    _, binary_img = cv2.threshold(magnitude, 50, 255, cv2.THRESH_BINARY)

    # Apply morphology operations to extract features
    kernel = np.ones((5, 5), np.uint8)
    opening = cv2.morphologyEx(binary_img, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(binary_img, cv2.MORPH_CLOSE, kernel)
    return magnitude

def fiberlyze(image_dir, hsv_step, mask_percentile, min_area, show):
    global info_out_df
    textStatusBar.set((f'Fiberlyzing {newpath.get()}'))
    hsv_step = int(hsv_step)
    min_area = min_area/100
    img = cv2.imread(image_dir,1)
    img = cv2.GaussianBlur(img, (7, 7), 0)
    height, width = img.shape[:2]

    # Define the desired width
    desired_width = 1000

    # Calculate the aspect ratio
    aspect_ratio = float(desired_width) / width

    # Calculate the desired height
    desired_height = int(height * aspect_ratio)

    # Resize the image
    resized = cv2.resize(img, (desired_width, desired_height))

    # stepwise HSV filtering
    mask_in = mask_hsv.create_hsv_mask(resized, hsv_step)
    mask_p = np.percentile(mask_in.flatten(), mask_percentile)

    # threshold to make mask
    threshed = np.zeros_like(mask_in)
    threshed[np.where(mask_in > mask_p)] = 255

    # show masked image
    if show == 1:
        masked = cv2.bitwise_and(resized, resized, mask=threshed)
        cv2.namedWindow("mask", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)

        cv2.imshow('mask', masked)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    # define connected component filter parameters
    thresh_vars = ((3, 10000), (3, 10000), (20, 10000000))
    connectivity = 4

    # find connected components
    mask_out, output, labels_thresh = connected_components.connect_components(threshed, connectivity, thresh_vars)
    output_label = np.zeros_like(output[1])
    output_label[np.isin(output[1], labels_thresh)] = output[1][np.isin(output[1], labels_thresh)]
    output_stats = []
    for i in labels_thresh:
        output_stats.append(output[2][i])

    output_thresh = (len(labels_thresh), output_label, np.array(output_stats), np.array(labels_thresh))

    # find skeletons

    info_out = skeletonizations.skeletonize_count_shortest_path2(output_thresh, mask_out,min_area)
    info_out_df = pd.DataFrame(info_out).transpose()
    info_out_df.columns = ['fiber_length', 'fiber_width',
                               'fiber_factor', 'fiber_branch', 'fiber_area']
    info_out_df = info_out_df[info_out_df.fiber_factor != 0]

    mean_factor = round(info_out_df["fiber_factor"].mean(),1)
    textStatusBar.set((f'Finished fiberlyzing {newpath.get()}, {len(info_out_df.index)} fibers measured,'
                       f' mean score = {mean_factor}'))

    image_size = desired_height*desired_width
    print(f'The mean fiber factor of {newpath.get()} is {mean_factor}, image size: {desired_height*desired_width}')

    return info_out_df, image_size

def plot():
    fig, axs = plt.subplots(1,3)
    axs[0].hist(info_out_df['fiber_factor'], bins=10)
    axs[0].set_title('fiber factor distribution')
    axs[1].hist(info_out_df['fiber_branch'], bins=10)
    axs[1].set_title('fiber branch distribution')
    axs[2].hist(info_out_df['fiber_area'], bins=10)
    axs[2].set_title('fiber area distribution')
    fig.show()

def save_raw_data():
    save_default = newpath.get().split('.')[0]
    saveFile = filedialog.asksaveasfilename(title="Save results",
                                            initialfile=save_default,
                                            defaultextension=".csv")
    if saveFile == None:
        textStatusBar.set(("Select a file to save to"))
        statusbar.configure(background='grey')
    else:
        textStatusBar.set(("Saving data to: ", saveFile))
        info_out_df.to_csv(saveFile, index=False)

# Row 1: Select file
lb_browse = Label(window, text="Select a Picture:")
bt_browse = Button(window, text="Open to browse", command=browseFiles)
lb_browse.grid(row=1, column=1, pady=5, padx=20, sticky=W)
bt_browse.grid(row=1, column=2, pady=5, padx=20, sticky=W)

# Row 2:
lb_hsv_step = Label(window, text="HSV Step")
lb_hsv_step.grid(row=2, column=1, pady=5, padx=20, sticky=W)

en_hsv_step = Entry(window, textvariable=hsv_step)
en_hsv_step.bind('<KeyPress>', lambda e: is_float(e, hsv_step))
en_hsv_step.grid(row=2, column=2, pady=5, padx=5, sticky=W)

# Row 3:
lb_mask_percentile = Label(window, text="Threshold")
lb_mask_percentile.grid(row=3, column=1, pady=5, padx=20, sticky=W)

en_mask_percentile = Entry(window, textvariable=mask_percentile)
#en_mask_percentile.bind('<KeyPress>', lambda e: is_float(e, mask_percentile))
en_mask_percentile.grid(row=3, column=2, pady=5, padx=5, sticky=W)

# Row 4:
lb_min_area = Label(window, text="Minimal fiber area (%)")
lb_min_area.grid(row=4, column=1, pady=5, padx=20, sticky=W)

en_min_area = Entry(window, textvariable=min_area)
#en_min_area.bind('<KeyPress>', lambda e: is_float(e, min_area))
en_min_area.grid(row=4, column=2, pady=5, padx=5, sticky=W)

# Row 5:
show_check = Checkbutton(window, text='Show mask', variable=show_plot, onvalue=1, offvalue=0)
show_check.grid(row=5, column=1, pady=5, padx=20, sticky=W)

bt_fiberlyze = Button(window, text="Fiberlyze", command=lambda: fiberlyze(path_fiber.get(),
                                                                               hsv_step.get(),
                                                                               mask_percentile.get(),
                                                                               min_area.get(),
                                                                               show_plot.get()))
bt_fiberlyze.grid(row=5, column=2, pady=5, padx=20, sticky=W)

# Row 6:
bt_plot = Button(window, text="Plot results", command=plot)
bt_plot.grid(row=6, column=1, pady=5, padx=20, sticky=W)

bt_plot = Button(window, text="Save results", command=save_raw_data)
bt_plot.grid(row=6, column=2, pady=5, padx=20, sticky=W)

# Row 7
credit = Label(window, text='Designed and developed by Food Process Engineering (WUR)')
credit.grid(row=7, column=1, columnspan = 2, pady=40, padx=20, sticky=W)
# status bar

statusbar = Label(window, textvariable=textStatusBar, relief=SUNKEN, anchor=W, pady=2, padx=10)
statusbar.place(rely=1, relx=0.5, anchor=S, relwidth=1)

window.mainloop()